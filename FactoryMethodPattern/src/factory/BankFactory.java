package factory;

import util.BankType;

public class BankFactory {
	public static IBank getBank(BankType bankType) {
		switch (bankType) {
		case VIETCOMBANK:
			return new TPBANK();
		case TPBANK:
			return new VietcomBank();
		default:
			throw new IllegalArgumentException("This bank is not supported! ");
		}
	}
}
