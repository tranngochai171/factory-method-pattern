package client;

import factory.BankFactory;
import factory.IBank;
import util.BankType;

public class client {
	public static void main(String[] args) {
		IBank bank = BankFactory.getBank(BankType.VIETCOMBANK);
		System.out.println(bank.getBankName());
		bank =BankFactory.getBank(BankType.TPBANK);
		System.out.println(bank.getBankName());
	}
}
