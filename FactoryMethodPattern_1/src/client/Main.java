package client;

import factory.AnimalFactory;
import factory.IAnimal;

public class Main {
	public static void main(String[] args) {
		IAnimal animal = null;
		for (int i = 0; i < 5; i++) {
			animal = AnimalFactory.createRandomAnimal();
			System.out.println(animal.getName());
		}

	}
}
