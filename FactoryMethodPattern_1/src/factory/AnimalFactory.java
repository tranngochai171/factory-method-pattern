package factory;

public class AnimalFactory {
	public static IAnimal createRandomAnimal() {
		int randomNumber = (int) (Math.random() * 3);
		switch (randomNumber) {
		case 0:
			return new Cat();
		case 1:
			return new Dog();
		default:
			return new Duck();
		}
	}
}
